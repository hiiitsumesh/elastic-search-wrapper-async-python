import logging
from typing import Any

from elasticsearch import AsyncElasticsearch
from elasticsearch.exceptions import NotFoundError


def handle_errors(func):
    async def wrapper(*args, **kwargs):
        try:
            return await func(*args, **kwargs)
        except NotFoundError as ex:
            # we can classify the exception later
            return dict(success=False, reason=', '.join(filter(lambda x: isinstance(x, str), ex.args)))

    return wrapper


class ElasticSearch(object):

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        self.els = AsyncElasticsearch(*args, **kwargs)

    async def format_and_send_logs(self, error_resp):
        message = f'{error_resp.get("type", "")} | {error_resp.get("reason", "")}'

        # need to send it to logger or kibana for debugging
        logging.critical(message)
        return None

    async def format_response(self, resp: dict) -> dict:
        if 'error' in resp:
            await self.format_and_send_logs(resp)
            return dict(success=False, reason=None)
        return dict(success=True, data=resp)

    async def health(self):
        return await self.els.cluster.health()

    async def get(self, index: str, id: str, params: list = None, headers=None) -> dict:
        return await self.els.get(index, id, params, headers)

    async def add_document(self, index: str, body: dict, type: str = 'default') -> dict:
        resp: dict = await self.els.index(index, body, type)
        resp: dict = await self.format_response(resp)

        return resp

    @handle_errors
    async def search_documents(self, body=None, index=None, params=None, headers=None) -> dict:
        resp: dict = await self.els.search(body, index)
        resp: dict = await self.format_response(resp)
        return resp

    async def create_prefix_query(self, keywords):
        return {
            "query": {
                "match": {
                    "search_keywords": keywords
                }
            }
        }

    async def create_fuzzy_query(self, keywords):
        return {
            "query": {
                "bool": {
                    "should": [
                        {
                            "prefix": {
                                "search_keywords": keywords
                            }
                        },
                        {
                            "fuzzy": {
                                "search_keywords": {
                                    "value": keywords,
                                    "boost": 1,
                                    "fuzziness": 2,
                                    "prefix_length": 0
                                }
                            }
                        },
                        # {
                        #   "match": {
                        #     "search_keywords": {
                        #       "query": keywords,
                        #       "operator": "or"
                        #     }
                        #   }
                        # },
                    ]
                }
            }
        }

    async def get_suggestions(self, keywords: str):
        query = await self.create_fuzzy_query(keywords)
        resp: dict = await self.els.search(body=query)
        resp: dict = await self.format_response(resp)
        return resp

    async def close(self):
        await self.els.close()
