import logging
import traceback

from sanic.app import Sanic
from sanic.response import json

from . import constants
from .utils import get_failure_response


async def not_found(request, exception) -> dict:
  resp: dict = await get_failure_response()
  resp['status'] = 400
  resp['message'] = constants.NOT_FOUND
  return json(resp)


async def internal_server_error(request, exception) -> dict:
  log_message = traceback.format_exc()

  logging.critical(log_message)
  
  message = constants.INTERNAL_SERVER_ERROR
  if Sanic.get_app().config.ENVIRONMENT != 'prod':
    message += f': {exception} | {log_message}'
  
  resp: dict = await get_failure_response()
  resp['status'] = 500
  resp['message'] = message
  return json(resp)


async def method_not_allowed(request, exception) -> dict:
  
  resp: dict = await get_failure_response()
  resp['status'] = 405
  resp['message'] = f'Method `{request.method}` not supported'
  return json(resp)
