from sanic import Sanic

from settings import APP_NAME

from . import constants

messages = {
  400: constants.INVALID_DATA,
  500: constants.INTERNAL_SERVER_ERROR,
}


async def get_current_app():
  return Sanic.get_app(APP_NAME)


async def get_success_response():
  return dict(
      success=True,
      message=constants.SUCCESS,
      status=200,
      data=dict()
  )

async def get_validation_error_response():
  return dict(
      success=False,
      message=constants.INVALID_DATA,
      status=400,
      data=dict()
  )

async def get_failure_response():
  return dict(
      success=False,
      message=constants.INTERNAL_SERVER_ERROR,
      status=500,
      data=dict()
  )


async def format_response(response: dict) -> dict:
  app = await get_current_app()
  if app.config.ENVIRONMENT != 'prod':
    return response
  
  response['message'] = messages[response['status']]
  response['data'] = {}
  return response
