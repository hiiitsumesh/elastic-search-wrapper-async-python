from sanic import Blueprint

from .controllers import health_check_bp

bp = Blueprint.group([health_check_bp], url_prefix='/healthy')
