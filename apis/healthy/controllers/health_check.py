from sanic import Blueprint
from sanic.request import Request
from sanic.response import json

bp = Blueprint('HealthCheck')


@bp.get('/')
async def health_check(request: Request):
    return json({"ok": "hai"})
