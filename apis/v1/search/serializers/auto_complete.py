import logging

from common.utils import format_response
from common.utils import get_success_response


async def get_formatted_response(resp: dict) -> dict:
    success_resp = await get_success_response()
    suggestions = []
    try:
        for obj in resp['data']['hits']['hits']:
            suggestions.append({"suggestion": obj['_source']['search_keywords']})
    except KeyError as ex:
        logging.critical(ex.args)
        pass
    success_resp['data'] = suggestions

    return await format_response(success_resp)
