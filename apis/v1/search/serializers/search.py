from common.utils import format_response
from common.utils import get_success_response


async def get_formatted_response(resp: dict) -> dict:
    success_resp = await get_success_response()
    success_resp['data'] = resp.get('data', {})

    if resp['success'] is False:
        success_resp['data'] = {}

    return await format_response(success_resp)
