from sanic import Blueprint
from sanic.request import Request
from sanic.response import json

from common.utils import get_current_app
from common.utils import get_success_response
from ..serializers.auto_complete import get_formatted_response

bp = Blueprint('AutoComplete')


@bp.get('/autocomplete')
async def index_data(request: Request):
    search_params = request.args.get('q', [])
    if not search_params:
        resp: dict = await get_success_response()
        resp['data'] = []
        return json(resp)

    keywords = search_params[0]

    app = await get_current_app()
    resp: dict = await app.ctx.es.get_suggestions(search_params)

    formatted_respose: dict = await get_formatted_response(resp)
    return json(formatted_respose)
