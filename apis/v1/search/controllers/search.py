from sanic import Blueprint
from sanic.request import Request
from sanic.response import json

from common.utils import get_current_app
from ..serializers.search import get_formatted_response
from ..validators.search import vaildate_search_params

bp = Blueprint('Search')


@bp.post('/')
async def search_data(request: Request):
    print(request.headers)
    print(request.json)
    success, data = await vaildate_search_params(request.json)

    if not success:
        return json(data)

    app = await get_current_app()
    resp: dict = await app.ctx.es.search_documents(
        index=data['index'],
        body=data['body']
    )

    formatted_respose: dict = await get_formatted_response(resp)
    return json(formatted_respose)
