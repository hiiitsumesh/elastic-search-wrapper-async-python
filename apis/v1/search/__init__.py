from sanic import Blueprint

from .controllers import auto_complete_bp
from .controllers import search_bp

bp = Blueprint.group([
    auto_complete_bp,
    search_bp
],
    url_prefix='/search',
    version='v1'
)
