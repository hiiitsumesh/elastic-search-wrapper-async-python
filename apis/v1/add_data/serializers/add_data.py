from common.utils import format_response
from common.utils import get_failure_response
from common.utils import get_success_response


async def get_formatted_response(resp: dict) -> dict:
    if resp['success'] is False:
        err_resp: dict = await get_failure_response()
        err_resp['data'] = resp.get('data')
        return await format_response(err_resp)

    success_resp = await get_success_response()
    success_resp['data'] = resp
    return await format_response(success_resp)
