from sanic import Blueprint
from sanic.request import Request
from sanic.response import json

from common.utils import get_current_app
from ..serializers.add_data import get_formatted_response
from ..validators.add_data import vaildate_new_search_data

bp = Blueprint('AddSearchData')


@bp.post('/')
async def add_data_in_indexer(request: Request) -> dict:
    success, data = await vaildate_new_search_data(request.json)

    if not success:
        return json(data)

    app = await get_current_app()
    resp: dict = await app.ctx.es.add_document(
        index=data['index'],
        body=data['body']
    )

    formatted_respose: dict = await get_formatted_response(resp)
    return json(formatted_respose)
