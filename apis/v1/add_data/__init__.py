from sanic import Blueprint

from .controllers import add_data_bp

bp = Blueprint.group([
    add_data_bp,
],
    url_prefix='/add-data',
    version='v1',
)
