from common.utils import format_response
from common.utils import get_validation_error_response

from .. import constants as local_constants


async def vaildate_new_search_data(raw_data: dict) -> dict:
    error_data = {}
    if not isinstance(raw_data, dict):
        error_resp: dict = await get_validation_error_response()
        error_resp['message'] = local_constants.INVALID_JSON
        return False, await format_response(error_resp)

    if not isinstance(raw_data.get('index'), str):
        error_data['index'] = local_constants.REQUIRED_FIELD

    if not isinstance(raw_data.get('type'), str):
        error_data['type'] = local_constants.REQUIRED_FIELD

    if not raw_data.get('body', {}).get('search_keywords'):
        error_data['body'] = {
            'search_keywords': local_constants.REQUIRED_FIELD
        }

    if error_data:
        error_resp: dict = await get_validation_error_response()
        error_resp['data'] = error_data
        return False, await format_response(error_resp)

    return True, raw_data
