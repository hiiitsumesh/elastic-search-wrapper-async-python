import asyncio

import pytest
from unittest.mock import AsyncMock
from sanic import Sanic
from server import app

from external_services.elastic_search import ElasticSearch

def test_healthy():
    client = app.test_client
    _, response = client.get('/healthy')


@pytest.fixture()
def mock_add_data_call(mocker):
    async_mock = AsyncMock(return_value=dict(success=True, data={}))
    mocker.patch('external_services.elastic_search.ElasticSearch.add_document', side_effect=async_mock)
    return async_mock


def test_add_data(mock_add_data_call):
    client = app.test_client
    data = {
        'index': 'search',
        'type': 'recent',
        'body': {
            'search_keywords': 'abc'
        }
    }
    _, response = client.post('/v1/add-data', json=data)
    assert response.json['data']['success'] is True