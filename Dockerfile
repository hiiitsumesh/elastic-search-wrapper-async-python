FROM python:3.8-buster

RUN useradd -ms /bin/bash  ubuntu

USER ubuntu:sudo
RUN mkdir -p /home/ubuntu/els_api

WORKDIR /home/ubuntu/els_api
COPY requirements.txt ./

RUN pip install -r requirements.txt
COPY  ./ ./
EXPOSE 8000
CMD [ "/home/ubuntu/.local/bin/uvicorn", "server:app", "--host=0.0.0.0", "--port=8000", "--workers=1", "--log-level=trace", "--access-log", "--use-colors"]