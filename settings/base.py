from os import getenv as e

ENVIRONMENT = e('ELS_API_ENVIRONMENT') or 'dev'

# elastic search service configuration
ELS_ENDPOINTS = [host for host in e('ELS_API_ELS_ENDPOINTS', '').split(', ') if host.strip()] or [
    'http://localhost:9200']
ELS_MAX_SIZE = e('ELS_API_ELS_MAX_SIZE') or 4  # max number of parallel connectin only useful in case of multithreading
