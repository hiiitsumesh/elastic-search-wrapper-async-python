import uvicorn
from sanic import Sanic
from sanic.exceptions import MethodNotSupported
from sanic.exceptions import NotFound
from sanic_cors import CORS

from apis.healthy import bp as health_bp
from apis.v1.add_data import bp as add_data_bp
from apis.v1.search import bp as search_bp
from common.exceptions import internal_server_error
from common.exceptions import method_not_allowed
from common.exceptions import not_found
from external_services.elastic_search import ElasticSearch
from settings import APP_NAME

# configure logging. 
log_config = uvicorn.config.LOGGING_CONFIG
log_config["formatters"]["access"]["fmt"] = "%(asctime)s - %(levelname)s - %(message)s"
log_config["formatters"]["default"]["fmt"] = "%(asctime)s - %(levelname)s - %(message)s"
# log_config = logging.basicConfig()  # for development only

app = Sanic(APP_NAME, load_env=False, log_config=log_config)

# cors are only for development purpose and wont be used behind the load balancer
cors = CORS(app, resources={r"/*": {"origins": "*"}})

# load app configuration
app.update_config('settings/base.py')

# register exception handlers
app.error_handler.add(NotFound, not_found)
app.error_handler.add(MethodNotSupported, method_not_allowed)
app.error_handler.add(Exception, internal_server_error)

# regiaster all api modules
app.blueprint(health_bp)
app.blueprint(add_data_bp)
app.blueprint(search_bp)


@app.listener("after_server_start")
async def app_start(app, *args):
    app.ctx.es = ElasticSearch(hosts=app.config.ELS_ENDPOINTS)


@app.listener("before_server_stop")
async def app_shutdown(app, *args):
    await app.ctx.es.close()


if __name__ == '__main__':
    app.run(host='0.0.0.0')
